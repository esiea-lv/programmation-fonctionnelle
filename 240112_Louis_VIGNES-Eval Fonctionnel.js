class Human {
    lastName;
    firstName;
    age;
    state;
    socials;
    immunity;

    constructor(firstName, lastName, age, state) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.state = state;
        this.socials = [];
        this.immunity = false;
    }

    addToSocials(person) {
        this.socials.push(person);
    }

    isSain() {
        return this.state === "sain";
    }

    /**
     * Infect with a virus.
     * @param virus :string
     */
    infectWith(virus) {
        this.state = virus;
    }
}

class Maybe {
    constructor(value) {
        this.value = value;
    }

    static of(value) {
        return new Maybe(value);
    }

    isNothing() {
        return this.value === null || this.value === undefined;
    }

    isSomething() {
        return !this.isNothing();
    }

    getOrElse(defaultValue) {
        return this.isNothing() ? defaultValue : this.value;
    }
}

const personnes = [];
// -----
const ludovic = new Human("Ludovic", "DUCHAMP", 53, "sain");
personnes.push(ludovic);
const aaron = new Human("Aaron", "BESCOND", 36, "sain");
ludovic.addToSocials(aaron);
const alexis = new Human("Alexis", "CHARBONNIER", 18, "sain");
const camille = new Human("Camille", "HENRY", 46, "sain");
aaron.addToSocials(alexis);
aaron.addToSocials(camille);
const alice = new Human("Alice", "DUBOIS", 45, "sain");
const clementine = new Human("Clémentine", "BOUTHILLIER", 80, "sain");
const celine = new Human("Céline", "JACQUET", 54, "sain");
const melanie = new Human("Mélanie", "D'AMBOISE", 20, "sain");
alexis.addToSocials(alice);
alexis.addToSocials(clementine);
camille.addToSocials(celine);
camille.addToSocials(melanie);
const antoine = new Human("Antoine", "ELIE", 25, "sain");
const aurelie = new Human("Aurélie", "FAURE", 47, "sain");
alice.addToSocials(antoine);
alice.addToSocials(aurelie);
// ---------
const david = new Human("David", "LEBLANC", 78, "sain");
personnes.push(david);
const eric = new Human("Éric", "ROUSSEAU", 67, "sain");
const julien = new Human("Julien", "NIEL", 65, "sain");
david.addToSocials(julien);
david.addToSocials(eric);
const solenne = new Human("Solenne", "BARBET", 89, "sain");
const ethan = new Human("Ethan", "THOMAS", 45, "sain");
const dominique = new Human("Dominique", "NOEL", 45, "sain");
const regine = new Human("Régine", "DELAUNAY", 55, "sain");
eric.addToSocials(solenne);
eric.addToSocials(ethan);
julien.addToSocials(dominique);
julien.addToSocials(regine);
const abraham = new Human("Abraham", "FIGUIER", 22, "sain");
const regis = new Human("Régis", "PERROT", 14, "sain");
dominique.addToSocials(abraham);
dominique.addToSocials(regis);

/**
 * Find in the list of all human, the human in top in social nodes of the human to find.
 * @param humanToFind {Human} the human to find his top in social nodes.
 * @param listOfHuman {Human[]} all the humans.
 * @returns {Human} the human in top.
 */
function findTopHuman(humanToFind, listOfHuman) {
    let topHuman;
    listOfHuman.forEach(humanInTop => {
        topHuman = searchIfContainsHuman(humanToFind, humanInTop);
        if (topHuman.isSomething()) {
            return topHuman.getOrElse(undefined);
        }
    });
    return topHuman.getOrElse(undefined);
}

/**
 * Search if a person has the person to find in his bottom social nodes.
 * @param humanToFind {Human} the person needed to be found in the bottom social nodes.
 * @param actualHuman {Human} the person actually being searched.
 * @param cache {string} Cache for memoize.
 * @returns {Maybe<Human>} the top person in nodes of the personne to find.
 */
function searchIfContainsHuman(humanToFind, actualHuman, cache = {}) {
    const cacheKey = `${humanToFind.lastName}_${humanToFind.firstName}_${actualHuman.lastName}_${actualHuman.firstName}`;
    if (cache[cacheKey]) {
        return Maybe.of(cache[cacheKey]);
    }
    for (let social of actualHuman.socials) {
        if (humanToFind.lastName === social.lastName && humanToFind.firstName === social.firstName) {
            cache[cacheKey] = actualHuman;
            return Maybe.of(actualHuman);
        }
        if (social.socials.length > 0) {
            let humanFound = searchIfContainsHuman(humanToFind, social, cache);
            if (humanFound.isSomething()) {
                cache[cacheKey] = humanFound.value;
                return humanFound;
            }
        }
    }
    return Maybe.of(null);
}

/**
 * Fonction d'ordre supérieur ici (prend 1 fonction en entrée)
 * Infect humans.
 * @param nameVirus name of the virus.
 * @param human {Human} human being infected.
 * @param listHuman {Human[]} list of all humans.
 * @param propagateAscending {boolean} true if virus should propagate in ascending nodes.
 * @param propagateDescending {boolean} true if virus should propagate in descending nodes.
 * @param propagateOnlyCurrentGroup {boolean} true if virus should propagate in only current group.
 * @param shouldInfect {function(human: Human): boolean} function to infect or not people.
 */
function infect(nameVirus, human, listHuman, propagateAscending, propagateDescending, propagateOnlyCurrentGroup, shouldInfect) {
    if (human.isSain() && !human.immunity && shouldInfect(human)) {
        human.infectWith(nameVirus);
        if (propagateDescending) {
            // Infect human in bottom nodes
            human.socials.forEach(humanInSocial => infect(nameVirus, humanInSocial, listHuman, propagateAscending, propagateDescending, propagateOnlyCurrentGroup, shouldInfect));
        }
        if (propagateAscending) {
            // Infect human in top nodes
            const topPerson = findTopHuman(human, listHuman)
            if (topPerson) {
                infect(nameVirus, topPerson, listHuman, propagateAscending, propagateDescending, propagateOnlyCurrentGroup, shouldInfect);
            }
        }
        if (propagateOnlyCurrentGroup) {
            human.socials.forEach(humanInGroup => {
                if (humanInGroup.isSain && shouldInfect(humanInGroup)) human.infectWith(nameVirus);
            })
        }
    }
}

/**
 * Fonction d'ordre supérieur ici (prend 2 fonctions en entrée)
 * Vaccinate all humans with a vaccine.
 * @param listHuman {Human[]} all humans.
 * @param listVirus {string[]} virus that the vaccine can heal.
 * @param heal {boolean} true if vaccine can heal.
 * @param immune {boolean} true if vaccine can immune.
 * @param sideEffect {function(human: Human) : void} apply effects to human.
 * @param effective {function(human: Human) : boolean} tells if the vaccine is working on the human.
 */
function vaccinate(listHuman, listVirus, heal, immune, sideEffect, effective) {
    listHuman.forEach(human => {
        if (heal && listVirus.indexOf(human.state) !== -1 && effective(human)) {
            human.isSain();
        }
        if (immune) {
            human.immunity = true;
        }
        sideEffect(human);
        if (human.socials.length > 0) {
            vaccinate(human.socials, listVirus, heal, immune, sideEffect, effective);
        }
    })
}

/**
 * Infect humans with variant A (only bottom nodes infected).
 * @param human {Human} the human to infect.
 * @param listHuman {Human[]} all humans.
 */
function infectUsingBasicVirus(human, listHuman) {
    infect("Zombie-19", human, listHuman, true, true, false, () => true)
}

/**
 * Infect humans with variant A (only bottom nodes infected).
 * @param human {Human} the human to infect.
 * @param listHuman {Human[]} all humans.
 */
function infectUsingVariantA(human, listHuman) {
    infect("Zombie-A", human, listHuman, false, true, false, () => true)
}

/**
 * Infect humans with variant B (only top nodes infected).
 * @param human {Human} the human to infect.
 * @param listHuman {Human[]} all humans.
 */
function infectUsingVariantB(human, listHuman) {
    infect("Zombie-B", human, listHuman, true, false, false, () => true)
}

/**
 * Infect humans with variant 32 (only 32 y.o. or more infected).
 * @param human {Human} the human to infect.
 * @param listHuman {Human[]} all humans.
 */
function infectUsingVariant32(human, listHuman) {
    infect("Zombie-32", human, listHuman, true, true, false, (human) => human.age >= 32)
}

/**
 * Infect humans with variant C (50% to be infected).
 * @param human {Human} the human to infect.
 * @param listHuman {Human[]} all humans.
 */
function infectUsingVariantC(human, listHuman) {
    infect("Zombie-C", human, listHuman, false, false, true, (human) => Math.random() >= 0.5)
}

/**
 * Infect humans with variant ultime (only top person of node is infected).
 * @param human {Human} the human to infect.
 * @param listHuman {Human[]} all humans.
 */
function infectUltime(human, listHuman) {
    // Too much particular to refactor
    let topHuman = human;
    let oldTopHuman = human;
    while (topHuman) {
        oldTopHuman = topHuman;
        topHuman = findTopHuman(topHuman, listHuman);
    }
    if (human.state === "sain" && !oldTopHuman.immunity) oldTopHuman.state = "Zombie-Ultime";
}

/**
 * Vaccinate humans using the A1 Vaccine.
 * @param listHuman {Human[]} humans to vaccine.
 */
function vaccinateA1(listHuman) {
    vaccinate(listHuman, ["Zombie-A", "Zombie-32"], true, true, () => {
    }, (human) => human.age <= 30)
}

/**
 * Vaccinate humans using the B1 Vaccine.
 * @param listHuman {Human[]} humans to vaccine.
 */
function vaccinateB1(listHuman) {
    vaccinate(listHuman, ["Zombie-B", "Zombie-C"], true, false, (human) => {
        if (Math.random() >= 0.5) human.state = "mort"
    }, () => true)
}

/**
 * Vaccinate humans using tha Ultime Vaccine.
 * @param listHuman {Human[]} humans to vaccine.
 */
function vaccinateUltime(listHuman) {
    vaccinate(listHuman, ["Zombie-Ultime"], true, true, () => {
    }, () => true)
}


// Uncomment what you want to do

// vaccinateA1(personnes);
// vaccinateB1(personnes);
// vaccinateUltime(personnes);

// infectUsingBasicVirus(dominique, personnes);
// infectUsingVariantA(dominique, personnes);
// infectUsingVariantB(dominique, personnes)
// infectUsingVariant32(dominique, personnes)
// infectUsingVariantC(dominique, personnes)
// infectUltime(dominique, personnes);

console.log(JSON.stringify(personnes));







